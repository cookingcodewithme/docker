build-letsencrypt:
	@docker build -t registry.gitlab.com/cookingcodewithme/docker:letsencrypt ./letsencrypt

push-letsencrypt:
	@docker push registry.gitlab.com/cookingcodewithme/docker:letsencrypt

build-newrelic:
	@docker build -t registry.gitlab.com/cookingcodewithme/docker:newrelic ./newrelic

push-newrelic:
	@docker push registry.gitlab.com/cookingcodewithme/docker:newrelic

clean:
	@docker rmi -f $(shell docker images -q)
