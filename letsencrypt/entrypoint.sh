#!/bin/bash

set -o errexit
set -o nounset
#set -o pipefail

if [ -z $HOSTS ];then
  echo "Please add -e HOST='' to docker run command."
  exit 1
fi

if [ -z $EMAIL ];then
  echo "Pleaes add -e EMAIL=''. to docker run command"
  exit 1
fi

echo "###HOSTS=$HOSTS"
echo "###EMAIL=$EMAIL"

TEST_CERT=""
if [ ! -z ${TEST+x} ];then
  TEST_CERT=" --test-cert"
  echo "###TEST_CERT is on."
else
  echo "###TEST_CERT is off."
fi

/nginx.conf.sh $HOSTS > /etc/nginx/nginx.conf

localhost=$(curl http://169.254.169.254/latest/meta-data/local-ipv4 --connect-timeout 2) && echo "###local=$localhost";sed -i "s/127.0.0.1/$localhost/g" /etc/nginx/nginx.conf

echo "###BEGIN:/etc/nginx/nginx.conf"
cat /etc/nginx/nginx.conf
echo "###END:/etc/nginx/nginx.conf"

for HOST in ${HOSTS//;/ }
do
  #"https://api.example.com:8080=http://127.0.0.1:8081;https://api2.example.com:8081=http://127.0.0.1:8082"

  PROTOCOL_DNS_PORT=${HOST/*=}
  PROTOCOL=${PROTOCOL_DNS_PORT/:\/\/*}
  echo "PROTOCOL=$PROTOCOL"
  DNS_PORT=${PROTOCOL_DNS_PORT/*:\/\/}

  DNS_NAME=${DNS_PORT/:*}
  echo "DNS_NAME=${DNS_NAME}"
  PORT=${DNS_PORT/*:}

  if [ "$DNS_PORT" = "$PORT" ];then
    PORT=""
  fi

  if [ "$PROTOCOL" = "https" ];then
    wget https://dl.eff.org/certbot-auto
    chmod a+x /certbot-auto
    break
  fi
done;

# if archinve folder already has the DNS directory, the DNS will not request to issue
for HOST in ${HOSTS//;/ }
do
  PROTOCOL_DNS_PORT=${HOST/*=}
  PROTOCOL=${PROTOCOL_DNS_PORT/:\/\/*}
  #echo "PROTOCOL=$PROTOCOL"
  DNS_PORT=${PROTOCOL_DNS_PORT/*:\/\/}

  DNS_NAME=${DNS_PORT/:*}
  #echo "DNS_NAME=${DNS_NAME}"
  PORT=${DNS_PORT/*:}

  if [ "$DNS_PORT" = "$PORT" ];then
    PORT=""
  fi

  # Checks PROTOCOL type from HOST information
  if [ "https" = "$PROTOCOL" ];then
    if [ ! -d /etc/letsencrypt/archive/${HOST/*=} ];then
      echo "### Requesting a SSL for ${HOST/*=}"
      /certbot-auto certonly --standalone -d ${HOST/*=} --debug --noninteractive --agree-tos $TEST_CERT --email $EMAIL
    fi
  fi
done

###/certbot-auto renew --quiet --no-self-upgrade --noninteractive

### nginx running on forground mode
nginx
