# Nginx + Let's Encrypt

## Reference
https://www.nginx.com/
https://letsencrypt.org/

## Build
```
docker build -t registry.gitlab.com/cookingcodewithme/docker:letsencrypt ./
```

## Execute
```
docker run --name=docker:letsencrypt \
      -e HOST=https://api.example.com:8080=http://127.0.0.1:8081;https://api2.example.com:8081=http://127.0.0.1:8082 \
      -e EMAIL="cookingcodewithme@gmail.com"
```
