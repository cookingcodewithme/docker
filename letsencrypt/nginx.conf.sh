#!/bin/bash

function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

HOSTS=${1:-}

cat <<EOF
user www-data;
worker_processes 4;
pid /run/nginx.pid;
daemon off;

events {
        worker_connections 768;
        # multi_accept on;
}

http {

        ##
        # Basic Settings
        ##

        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
        keepalive_timeout 180;
        types_hash_max_size 2048;

        # server_tokens off;

        # server_names_hash_bucket_size 64;
        # server_name_in_redirect off;

        include /etc/nginx/mime.types;
        default_type application/octet-stream;

        ##
        # Logging Settings
        ##

        log_format timed_combined '\$remote_addr - \$remote_user [\$time_local] '
                '"\$request" \$status \$body_bytes_sent '
                '"\$http_referer" "\$http_user_agent" '
                '\$request_time \$upstream_response_time \$pipe '
                '\$request_body';

        access_log /var/log/nginx/access.log timed_combined;
        error_log /var/log/nginx/error.log;

        ##
        # Gzip Settings
        ##

        gzip on;
        gzip_disable "msie6";

        # gzip_vary on;
        # gzip_proxied any;
        # gzip_comp_level 6;
        # gzip_buffers 16 8k;
        # gzip_http_version 1.1;
        # gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;
        ##
        # nginx-naxsi config
        ##
        # Uncomment it if you installed nginx-naxsi
        ##

        #include /etc/nginx/naxsi_core.rules;

        ##
        # nginx-passenger config
        ##
        # Uncomment it if you installed nginx-passenger
        ##

        #passenger_root /usr;
        #passenger_ruby /usr/bin/ruby;

        ##
        # Virtual Host Configs
        ##

        #include /etc/nginx/conf.d/*.conf;
        #include /etc/nginx/sites-enabled/*;

EOF

for HOST in ${HOSTS//;/ }
do
  PROXY_PASS=${HOST/=*}
  PROTOCOL_DNS_PORT=${HOST/*=}
  PROTOCOL=${PROTOCOL_DNS_PORT/:\/\/*}

  DNS_PORT=${PROTOCOL_DNS_PORT/*:\/\/}

  DNS_NAME=${DNS_PORT/:*}
  PORT=${DNS_PORT/*:}

  if valid_ip ${PROXY_PASS/:*}; then
    RESOLVER=""
  else
    RESOLVER="resolver 8.8.8.8 valid=30s;"
  fi

  if [ "http" = "$PROTOCOL" ];then
cat <<EOF
        server {
          listen ${PORT:-80};
          listen [::]:${PORT:-80};
          server_name $DNS_NAME;
          location / {
            client_max_body_size 10m;
            proxy_connect_timeout 600;
            proxy_send_timeout 600;
            proxy_read_timeout 600;
            send_timeout 600;
            proxy_set_header X-Real-IP \$remote_addr;
#           proxy_set_header Host \$host;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            proxy_pass $PROXY_PASS;
            $RESOLVER
          }
        }
EOF
  else
cat <<EOF
        server {
          listen 80;
          listen [::]:80;
          server_name $DNS_NAME;
          return 301 https://\$server_name\$request_uri;
        }

        server {
          listen 443 ssl;
          listen [::]:443 ssl;
          server_name $DNS_NAME;
          ssl_certificate "/etc/letsencrypt/live/$DNS_NAME/fullchain.pem";
          ssl_certificate_key "/etc/letsencrypt/live/$DNS_NAME/privkey.pem";
          ssl_session_cache shared:SSL:10m;
          ssl_session_timeout  10m;
          ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
          ssl_ciphers HIGH:SEED:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!RSAPSK:!aDH:!aECDH:!EDH-DSS-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA:!SRP;
          ssl_prefer_server_ciphers on;
          include /etc/nginx/default.d/*.conf;

          location / {
            client_max_body_size 10m;
            proxy_connect_timeout 600;
            proxy_send_timeout 600;
            proxy_read_timeout 600;
            send_timeout 600;
            proxy_set_header X-Real-IP \$remote_addr;
#           proxy_set_header Host \$host;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            proxy_pass $PROXY_PASS;
            $RESOLVER
          }
        }
EOF
  fi
done

cat <<EOF
}
#mail {
#       # See sample authentication script at:
#       # http://wiki.nginx.org/ImapAuthenticateWithApachePhpScript
#
#       # auth_http localhost/auth.php;
#       # pop3_capabilities "TOP" "USER";
#       # imap_capabilities "IMAP4rev1" "UIDPLUS";
#
#       server {
#               listen     localhost:110;
#               protocol   pop3;
#               proxy      on;
#       }
#
#       server {
#               listen     localhost:143;
#               protocol   imap;
#               proxy      on;
#       }
#}
EOF
